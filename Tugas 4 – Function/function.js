console.log("===NUMBER 1===")
function teriak() {
    // console.log("Halo Sanbers!")
    var say = "Halo Sanbers!"
    return say
}
console.log(teriak())


console.log("===NUMBER 2===")
function kalikan(num1, num2) {
    var kali = num1*num2
    return kali
}
var num1 = 12
var num2 = 4
var hasilKali = kalikan(num1, num2)
console.log(hasilKali) // 48


console.log("===NUMBER 3===")
function introduce(name, age, address, hobby) {
    console.log("Nama saya "+name+", umur saya "+age+" tahun, alamat saya di "+address+", dan saya punya hobby yaitu "+hobby+"!")    
}
var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan)