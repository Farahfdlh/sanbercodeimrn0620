import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Image } from 'react-native';


export default class App extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                {/* <Text style={styles.tabTitle}>Hammasah!  :)</Text> */}

                <View style={styles.box2} >
                    {/* <Image source={require('./image/capture.png')} style={{ width: 98, height: 22 }} /> */}

                    <View style={styles.boxEm} >
                        <Text style={styles.texBox}>Email</Text>
                    </View>

                    <View style={styles.boxPass}>
                        <Text style={styles.texBox}>Password</Text>
                    </View>
                </View>

                <TouchableOpacity style={styles.box} >
                </TouchableOpacity>
                <Text style={styles.texlog}>Login</Text>
            </View>
        )
    }
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#000',
        // alignItems: 'center',
        // justifyContent: 'center',
    },
    box: {  //boxlogin
        width: 278,
        height: 35,
        backgroundColor: '#A39393',
        // borderWidth: 2,
        // borderColor: 'steelblue',
        borderRadius: 10,
        // borderTopWidth: 5,
        // paddingTop: 278, //lebar
        left: 41,
        top: 150,
        // alignItems: 'center',
        // justifyContent: 'center',
    },
    box2: {  //big box
        width: 305,
        height: 363,
        backgroundColor: '#E5E5E5',
        borderRadius: 10,
        left: 28,
        top: 105,
        alignItems: 'center',
    },
    boxEm: {
        width: 256,
        height: 38,
        backgroundColor: '#FFF',
        borderRadius: 10,
        // left: 21,
        top: 240,
        borderWidth: 1,
        borderColor: '#A59898',
        // alignItems: 'center',
        // textAlign: 'center'

    },
    boxPass: {
        width: 256,
        height: 38,
        backgroundColor: '#FFF',
        borderRadius: 10,
        // left: 21,
        top: 260,
        borderWidth: 1,
        borderColor: '#A59898',

    },
    texBox: {
        fontSize: 16,
        color: '#A59898',
        left: 20,
        top: 5
    },
    texlog: {
        fontSize: 18,
        color: '#fff',
        // paddingTop: 278,
        left: 150,
        top: 118
        // paddingBottom: 100,
        // width: 45,
        // height: 21,
    },
});