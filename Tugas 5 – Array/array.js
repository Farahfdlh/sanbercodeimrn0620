console.log("====no. 1=====")
function range(startNum, finishNum) {
    var arr = [];
    if (finishNum == null) {
        arr = -1
    } else {
        if (startNum > finishNum) {
            for (let i = startNum; i >= finishNum; i--) {
                arr.push(i);
            }
        } else {
            for (let i = startNum; i <= finishNum; i++) {
                arr.push(i);
            }
        }
    }

    return arr;
}
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11, 18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

console.log("====no. 2=====")
function rangeWithStep(startNum, finishNum, step) {

    var arr2 = [];
    if (startNum > finishNum) {
        for (let i = startNum; i >= finishNum; i -= step) {
            arr2.push(i);
        }
    } else {
        for (let i = startNum; i <= finishNum; i += step) {
            arr2.push(i);
        }
    }
    return arr2;

}
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 


console.log("====no. 3=====")
function sum(startNum, finishNum, step) {
    var x = [];
    if (startNum == null) {
        return (0);
    }
    if (step > 0) {
        for (var y = startNum; y <= finishNum; y += step) {
            x.push(y)

        }
    } else {
        for (var z = startNum; z >= finishNum; z += step) {
            x.push(z)
        }
    } 
    var ooo = x.reduce((a, b) => a + b, 1);
    return ooo
}
console.log(sum(1, 10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15, 10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) //0


console.log("====no. 4=====")
function dataHandling() {
    var input = [
        ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
        ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
        ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
        ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
    ]
    for (i = 0; i < input.length; i++) {
        console.log("Nomor ID: " + input[i][0] + "\nNama Lengkap: " + input[i][1] + "\nTTL: " + input[i][2] + " " + input[i][3] + "\n Hobi: " + input[i][4] + "\n")
    }
}

console.log(dataHandling())


console.log("====no. 5=====")
function balikKata(str) {
    // return str.split('').reverse().join('')
    var newstr = ''
    for (i = str.length - 1; i >= 0; i--) {
        newstr += str[i]
    }
    return newstr
}
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 


console.log("\n====no. 6=====")
function dataHandling2() {

    var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
    // console.log(input);

    var nama = input[1].split('');
    nama.push('Elsharawy');
    input.splice(1, 1, nama.join(''));

    var provinsi = input[2].split('')
    provinsi.unshift('Provinsi ')
    input.splice(2, 1, provinsi.join(''));

    input.splice(4, 1, 'Pria', 'SMA Internaisonal Metro');

    console.log(input);

    var changeMoon = input[3].split('/');
    var bulan = parseInt(changeMoon[1]);

    switch (bulan) {
        case 01: { bulan = 'Januari'; break; }
        case 02: { bulan = 'Februari'; break; }
        case 03: { bulan = 'Maret'; break; }
        case 04: { bulan = 'April'; break; }
        case 05: { bulan = 'Mei'; break; }

    }
    console.log(bulan);

    var sort = changeMoon
    changeMoon.sort(function (a, b) { return b - a }) //sort descending
    console.log(changeMoon)

    var gantiStrip = input[3].split('/');
    console.log(gantiStrip.join('-'));

    var namaAja = input[1].split('')
    namaAja.splice(15, 10)
    console.log(namaAja.join(''))
}
console.log(dataHandling2())
