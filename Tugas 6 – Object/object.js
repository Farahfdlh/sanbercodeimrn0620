console.log("=========No. 1 (Array to Object)===========")
function arrayToObject(arr) {
    var now = new Date()
    var thisYear = now.getFullYear() // 2020 (tahun sekarang)
    var objBio = {}

    if (arr == undefined) {
        return ' "" '
    } else {
        for (i = 0; i < arr.length; i++) {
            objBio = {
                firstName: arr[i][0],
                lastName: arr[i][1],
                gender: arr[i][2],
                age: thisYear - arr[i][3]
            }
            if (!objBio.age || objBio.age <= 0) {
                objBio.age = "Invalid Birth Year"
            }
            console.log(`${i + 1}. ${objBio.firstName} ${objBio.lastName} : {
                            firstName : ${objBio.firstName},
                            lastName : ${objBio.lastName},
                            gender : ${objBio.gender},
                            age : ${objBio.age}
                        }`)
        }
    }
    return arr
}
// Driver Code
var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]]
arrayToObject(people)
var people2 = [["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023]]
arrayToObject(people2)
// Error case 
arrayToObject([]) // ""


console.log("\n=========No. 2 (Shopping Time)===========")
function shoppingTime(memberId, money) {
    var memMon = (memberId, money)
    var memberID = memberId
    var kembalian = money
    var product = []

    if (memberID === '' || memMon === undefined) {
        return ("Mohon maaf, toko X hanya berlaku untuk member saja")
    } else if (money < 50000) {
        return ("Mohon maaf, uang tidak cukup")
    } else {

        var shopObj = {}
        shopObj.memberId = memberId
        shopObj.money = money

        if (kembalian >= 1500000) {
            kembalian -= 1500000
            product.push('Sepatu Stacattu')
        } if (kembalian >= 500000) {
            product.push('Baju Zoro')
            kembalian -= 500000
        } if (kembalian >= 250000) {
            product.push('Baju H&N')
            kembalian = kembalian - 250000
        } if (kembalian >= 175000) {
            product.push('Sweater Uniklooh')
            kembalian = kembalian - 175000
        } if (kembalian >= 50000) {
            product.push('Casing Handphone')
            kembalian = kembalian - 50000
        }
    }

    shopObj.listPurchased = product
    shopObj.changeMoney = kembalian
    return shopObj
}
// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja


console.log("=========No. 3 (Naik Angkot)===========")
function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var objPenumpang = {}
            var display = []


    if (arrPenumpang == undefined) {
        return "[]"
    } else {

        for (i = 0; i < arrPenumpang.length; i++) {
            var tujuannya = rute.indexOf(arrPenumpang[i][2])
            var start = rute.indexOf(arrPenumpang[i][1])
            var cash = (tujuannya - start) * 2000

            objPenumpang = {
                penumpang: arrPenumpang[i][0],
                naikDari: arrPenumpang[i][1],
                tujuan: arrPenumpang[i][2],
                bayar: cash
            }
            display.push(objPenumpang)
            // console.log(objPenumpang)
        }
        return display
    }
}

//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]