console.log("=====A=====")
function bandingkan(num1, num2) {
    if (num1 == null) {
        return -1
    } else if (num1 == 1) {
        return 1
    } else {
        if (num1 || num2 < 0) {
            return -1
        } else if (num1 == num2) {
            return -1
        } else if (num1 || num2 > 0) {
            return num2
        }
    }
}
// TEST CASES Bandingkan Angka
console.log(bandingkan(10, 15)); // 15 
console.log(bandingkan(12, 12)); // -1 
console.log(bandingkan(-1, 10)); // -1 
console.log(bandingkan(112, 121));// 121 
console.log(bandingkan(1)); // 1 
console.log(bandingkan()); // -1 
console.log(bandingkan("15", "18")) // 18


console.log("\n=====B=====")
function balikString(str) {
    var newstr = ''
    for (i = str.length - 1; i >= 0; i--) {
        newstr += str[i]
    }
    return newstr
}
// TEST CASES BalikString 
console.log(balikString("abcde")) // edcba 
console.log(balikString("rusak")) // kasur 
console.log(balikString("racecar")) // racecar 
console.log(balikString("haji")) // ijah

console.log("\n=====C=====")
function palindrome(string) {
    var newstr = ''
    // string[i] = string.charAt(string.length) -1
    for (i = string.length - 1; i >= 0; i--) {
        if (string[i] !== string[string.length - i - 1]) {
            return newstr = string

        } else {
            return newstr += string
        }
    }
    return newstr
}
// TEST CASES Palindrome 
console.log(palindrome("kasur rusak")) // true 
console.log(palindrome("haji ijah")) // true 
console.log(palindrome("nabasan")) // false 
console.log(palindrome("nababan")) // true 
console.log(palindrome("jakarta")) // false