console.log("\n=====A=====")
function DescendingTen(num) {
    numb = 0
    if (num == null) {
        return -1
    } else {
        for (i = num; i >= num - 10; i--) {
            return num[i]
        }
    }
    return num
}
// TEST CASES Descending Ten 
console.log(DescendingTen(100)) // 100 99 98 97 96 95 94 93 92 91 
console.log(DescendingTen(10)) // 10 9 8 7 6 5 4 3 2 1 
console.log(DescendingTen()) // -1

console.log("\n=====B=====")
function AscendingTen(num) {
    numb = 0
    if (num == null) {
        return String(-1)
    } else {
        for (i = num; i <= num + 10; i++) {
            // numb += num[i]
            return String(" " + num)
        }
    }
}
// TEST CASES Ascending Ten 
console.log(AscendingTen(11)) // 11 12 13 14 15 16 17 18 19 20 
console.log(AscendingTen(21)) // 21 22 23 24 25 26 27 28 29 30 
console.log(AscendingTen()) // -1

console.log("\n=====C=====")
function ConditionalAscDesc(reference, check) {
    if (reference == null || check == null) {
        return -1
    } else {
        if (check % 2 != 0) { //ganjil
            for (i = reference; i <= reference + 10; i++) { //assc
                // numb += num[i]
                return String(" " + reference)
            }
        } else {
            for (i = reference; i >= reference - 10; i--) { //dess
                return reference
            }
        }
    }
}
// TEST CASES Conditional Ascending Descending 
console.log(ConditionalAscDesc(20, 8)) // 20 19 18 17 16 15 14 13 12 11 
console.log(ConditionalAscDesc(81, 1)) // 81 82 83 84 85 86 87 88 89 90 
console.log(ConditionalAscDesc(31)) // -1 
console.log(ConditionalAscDesc()) // -1

console.log("\n=====D=====")
function ularTangga() {
    pp = 0
    for (i = 1; i <= 11; i++) { //kolomnya
        for (j = 1; j <= 10; j++) { //barisnya
            pp += j;
        }
        console.log(pp)
        pp = ''
    }
}
console.log(ularTangga())