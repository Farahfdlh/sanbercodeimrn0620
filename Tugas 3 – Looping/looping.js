var value = 1
console.log("===LOPING WHILE=== \nLOOPING PERTAMA")
while (value <= 20) {
    if (value % 2 == 0) {
        console.log(value + " - I love coding")
    }
    value++

}
var value2 = 20
console.log("LOOPING KEDUA")
while (value2 >= 1) {
    if (value2 % 2 == 0) {
        console.log(value2 + " - I will become a mobile developer")
    } value2--

}

console.log("\n===LOOPING FOR===")
for ( index = 1; index <= 20; index++) {
    //const element = array[index];
    if (index % 3 == 0 && index % 2 == 1) {               //kel.3nyaaaa    dont forgett
        console.log(index + " - I Love Coding ")
    } else if (index % 2 == 1) {  //ganjil
        console.log(index + " - Santai")
    }else if (index % 2 == 0) { //genap
        console.log(index + " - Berkualitas")
    }
}



console.log("\n===PERSEGI PANJANG===")
var pp = ''
for (i = 1; i <= 4; i++) { //kolomnya
    for (j = 1; j <= 8; j++) { //barisnya
        pp += "#";
    }
    console.log(pp)
    pp = ''
}

console.log("\n===TANGGA===")
var out = ''
for (i = 1; i <= 7; i++) {
    for (j = 1; j <= i; j++) {
        out += "#";
    }
    console.log(out)
    out = ''
}
/* cara 2
for (i= "#"; i.length <= 7; i += "#") {
    console.log(i)
}*/

console.log("\n===PAPAN CATUR===")
var output = '';
for (var i = 0; i <= 7; i++) {  //kolom (loop ke bawah)
    for (var j = 1; j <= 8; j++) { //baris (loop ke samping)
        output += '# ';
    }
    console.log(output);
    if (i % 2 == 0) {
        output = ' ';
    } else {
        output = '';
    }
}