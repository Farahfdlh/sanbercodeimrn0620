console.log("=====1. Fungsi Arrow=====")
goldenFunction = () => {
    //function
    console.log("this is golden!!")

}
const golden = () => goldenFunction()
golden()


console.log("\n=====2. Object literal=====")
// const newFunction = () => literal("William", "Imoh")    //CARA 2
const newFunction = () =>
    literal = (firstName, lastName) => {
        return {
            firstName,
            lastName,
            fullName: () => {
                console.log(`${firstName} ${lastName}`)
            }
        }
    }
//Driver Code 
// newFunction("William", "Imoh").fullName()   //CARA 2
newFunction()("William", "Imoh").fullName()



console.log("\n=====3. Destructuring=====")
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}
// Driver code
const { firstName, lastName, destination, occupation, spell } = newObject;

console.log(firstName, lastName, destination, occupation)


console.log("\n=====4. Array Spreading=====")
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
//const combined = west.concat(east)

//Driver Code
const combined = [...west, ...east]
console.log(combined)


console.log("\n=====5. Template Literals=====")
const planet = 'earth';
const view = 'glass';

var before = `Lorem ${view}dolor sit amet, 
    consectetur adipiscing elit, ${planet}do eiusmod tempor 
    incididunt ut labore et dolore magna aliqua. 
    Ut enim ad minim veniam`

// Driver Code
console.log(before)