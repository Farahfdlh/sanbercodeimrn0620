// di index.js
var readBooks = require('./callback.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
]

// Tulis code untuk memanggil function readBooks di sini
var times = 10000
function countDown(x) {
    if (x == books.length) {
        return 0
    } else {
        readBooks(times, books[x], function (check) {
            // times = times - books[x].timeSpent   //CARA 2
            times = check
            countDown(x+1)
        });
    }
}
countDown(0)