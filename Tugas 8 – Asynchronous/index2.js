var readBooksPromise = require('./promise.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
]

// Lanjutkan code untuk menjalankan function readBooksPromise
var times = 10000
function runPromise(x) {
    if (x == books.length) {
        return 0
    } else {
    readBooksPromise(times, books[x])
        .then(function (fulfilled) {
            // times = times - books[x].timeSpent   //CARA 2
            times = fulfilled
            runPromise(x+1)
        })
        .catch(function (reject) {
            console.log(reject)

        })
    }
}
runPromise(0)